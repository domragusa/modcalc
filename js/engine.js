class CalcALU{
    constructor(){
        this.acc = 0;
        this.mod = 10000000;
    }
    
    load(x){
        let m = this.mod;
        this.acc = (x%m+m)%m;
    }
    
    inv(){
        let m = CalcALU.gcd(this.acc, this.mod);
        if(m[0] == 1){
            this.load(m[1]);
        }else{
            throw "NOINV";
        }
    }
    
    add(x){
        this.load(this.acc + x);
    }
    
    sub(x){
        this.add(-x);
    }
    
    mul(x){
        this.load(this.acc * x);
    }
    
    div(x){
        let tmp = this.acc;
        this.load(x);
        this.inv();
        this.mul(tmp);
    }
    
    pow(n){
        if(n == 0 && this.acc == 0){
            throw "UNDEF";
        }else if(n < 0){
            this.inv();
            return this.pow(-n);
        }
        
        let tmp = this.acc,
            bin = n.toString(2);
        
        this.load(1);
        for(let i of bin){
            this.mul(this.acc);
            if(i=='1'){this.mul(tmp);}
        }
    }
    
    static gcd(a, b, m){
        let q = (a/b)|0;
        let r = (a%b+b)%b;
        if(!m){m = [0, 1, 1, -q];}
        m = [m[1], m[0]-m[1]*q, m[3], m[2]-m[3]*q];
        
        if(r == 0){
            return [b, m[2]];
        }else{
            return CalcALU.gcd(b, r, m);
        }
    }
}

export class CalcEngine{
    constructor(){
        this.alu = new CalcALU();
        this.memory = 0;
        this.value = 0;
        this.status = 'input'; // input | result | error
        this.error = undefined;
        this.operation = undefined;
        this.reduced = false;
        this.oldValue = 0;
    }
    
    storeState(){
        return JSON.stringify({
            mod: this.alu.mod,
            mem: this.memory,
            red: this.reduced
        });
    }
    
    loadState(data){
        if(!data) return;
        data = JSON.parse(data);
        if(data.mod) this.alu.mod = data.mod|0;
        if(data.mem) this.memory  = data.mem|0;
        if(data.red) this.reduced = data.red;
    }
    
    toggleFormat(){
        this.reduced = !this.reduced;
    }
    
    setError(message){
        this.status = 'error';
        this.error = message;
    }
    
    setModulo(){
        let x;
        if(this.error){
            return;
        }else if(this.status == 'result'){
            x = this.alu.acc;
        }else{
            x = this.value;
        }
        
        x = x|0;
        if(x < 2){
            this.setError('BADMOD');
            return;
        }
        
        this.clear();
        this.alu.mod = x;
    }
    
    loadMemory(){
        if(this.error || !this.memory) return;
        
        //this.clear();
        this.value = this.memory;
        this.status = 'input';
    }
    
    setMemory(){
        if(this.error) return;
        
        if(this.status == 'result'){
            this.memory = this.alu.acc;
        }else{
            this.memory = this.value;
        }
    }
    
    getDisplay(){
        let sym = {'add': '+', 'sub': '–', 'mul': '×', 'div': '÷', 'pow': '^'};
        let out = {};
        
        let accformatted = ()=>{
            let acc = this.alu.acc;
            if(this.reduced){ // reduced form
                return acc > this.alu.mod/2 ? acc-this.alu.mod : acc;
            }else{            // normal form
                return acc;
            }
        };
        
        if(this.error){
            out = {head: 'err:', main: this.error};
        }else if(this.status == 'input'){
            if(this.operation == undefined){
                out.head = '?';
            }else{
                out.head = this.oldValue + ' ' + sym[this.operation];
            }
            out.main = this.value;
        }else{
            if(this.operation){
                out.head = this.oldValue +' '+sym[this.operation]+' '+ this.value + ' =';
            }else{
                out.head = this.oldValue +' =';
            }
            out.main = accformatted();
        }
        
        out.mod = this.alu.mod;
        return out;
    }
    
    clear(all=false){
        this.value = 0;
        this.alu.acc = 0;
        if(all)this.alu.mod = 10000000;
        if(all)this.reduced = false;
        this.operation = undefined;
        this.status = 'input';
        this.error = undefined;
        this.oldValue = 0;
    }
    
    del(){
        if(this.error || this.status == 'result'){
            this.clear();
        }else{
            let tmp = this.value;
            tmp = (tmp/10)|0;
            this.value = tmp;
        }
    }
    
    putDigit(x){
        if(this.error) return;
        
        if(this.status == 'result'){
            this.clear();
        }
        
        let tmp = this.value*10;
//             if(tmp > 99999999 || tmp < -99999999) return;
//             loss of precision with 99999999**2
        if(tmp > 9999999 || tmp < -9999999) return;
        tmp += (tmp<0 ? -1 : 1) * x|0;
        this.value = tmp;
    }
    
    neg(){
        if(this.error || this.status == 'result') return;
        
        this.value *= -1;
    }
    
    setOperation(o){
        if(this.error) return;
        
        if(this.operation){
            if(this.status != 'result'){
                this.doCalc();
            }
            this.oldValue = this.alu.acc;
        }else{
            if(this.status == 'input'){
                this.oldValue = this.value;
                this.alu.load(this.value);
            }else{
                this.oldValue = this.alu.acc;
            }
        }
        
        this.value = 0;
        this.operation = o;
        this.status = 'input';
    }
    
    equals(){
        if(this.error) return;
        
        this.status = 'result';
        if(this.operation == undefined){
            this.oldValue = this.value;
            this.alu.load(this.value);
        }else{
            this.doCalc();
        }
    }
    
    doCalc(){
        if(!this.alu[this.operation]) return;
        
        try{
            this.oldValue = this.alu.acc;
            this.alu[this.operation](this.value);
        }catch(e){
            this.setError(e);
        }
    }
}
