export class CalcUI{
    constructor(engine){
        this.engine = engine;
        
        let elem = (name)=>{
            let tmp = document.createElement('div');
            if(name){tmp.className = name;}
            return tmp;
        };
        
        this.container = elem('calc');
        
        this.d_head = elem('dHead');
        this.d_main = elem('dMain');
        this.d_status = elem('dStatus');
        
        this.display = elem('display');
        this.display.appendChild(this.d_head);
        this.display.appendChild(this.d_main);
        this.display.appendChild(this.d_status);
        
        this.keypad = elem('keypad');
        
        this.container.appendChild(this.display);
        this.container.appendChild(this.keypad);
        
        this.leds = [];
        this.indicators = [];
        this.keys = {};
        
        window.addEventListener('resize', ()=>{this.update()});
    }
    
    loadLayout(cols, layout){
        let keypad = this.keypad;
        
        // remove old buttons
        while(keypad.hasChildNodes()){keypad.removeChild(keypad.firstChild)}
        this.leds = [];
        
        keypad.style.gridTemplateColumns = '1fr '.repeat(cols);
        for(let id of layout){
            this.addButton(id);
        }
        
        this.update();
    }
    
    registerButton({
        id=undefined,
        label='',
        cname=undefined,
        onPress=undefined,
        onLongpress=undefined,
        status=undefined,
        doubleWidth=false
    }){
        if(id in this.keys){throw 'registerButton: duplicate id: '+id;}
        
        this.keys[id] = {
            label: label,
            cname: cname,
            onPress: ()=>{onPress(); this.update();},
            onLongpress: ()=>{onLongpress(); this.update(); navigator.vibrate(50);},
            status: status,
            doubleWidth: doubleWidth
        };
    }
    
    addButton(id){
        let cur = this.keys[id];
        if(!cur){throw 'loadLayout: unknown button: ' + id;}
        
        let tag = document.createElement('button');
        tag.classList.add('key');
        tag.textContent = cur.label;
        
        if(cur.cname) tag.classList.add(cur.cname);
        if(cur.onPress) tag.addEventListener('click', cur.onPress);
        if(cur.onLongpress) longpress_helper(tag, cur.onLongpress);
        if(cur.status) this.leds.push([tag, cur.status]);
        if(cur.doubleWidth) tag.classList.add('keyDouble');
        
        this.keypad.appendChild(tag);
    }
    
    addIndicator(status, onPress, onLongpress){
        let tag = document.createElement('div');
        if(onPress) tag.addEventListener('click', ()=>{onPress(); this.update();},);
        if(onLongpress) longpress_helper(tag, ()=>{onLongpress(); this.update(); navigator.vibrate(50);});
        this.d_status.appendChild(tag);
        this.indicators.push([tag, status]);
    }
    
    update(){
        let out = this.engine.getDisplay();
        
        this.d_head.textContent = out.head;
        this.d_main.textContent = out.main;
        CalcUI.fitText(this.d_main);
        
        for(let [tag, fn] of this.leds){
            tag.classList.remove('ledOff', 'ledOn');
            tag.classList.add(fn() ? 'ledOn' : 'ledOff');
        }
        
        for(let [tag, fn] of this.indicators){
            tag.textContent = fn();
        }
    }
    
    static fitText(elem){
        elem.style.fontSize = '';
        let getSize = (prop)=>(parseFloat(getComputedStyle(elem)[prop]));
        let ratio = getSize('width')/elem.scrollWidth;
        if(ratio<0.98){
            elem.style.fontSize = getSize('fontSize')*(ratio-0.02) +'px';
        }
    }
}

function longpress_helper(tag, callback){
    let timer;
    tag.addEventListener('touchend', ()=>{clearTimeout(timer)});
    tag.addEventListener('touchstart', ()=>{timer=setTimeout(callback, 1000)});
}

