import { CalcEngine } from './engine.js';
import { CalcUI } from './ui.js';


function loadDefaultCalc(calc){
    let keys = {
        '0': {label: '0', onPress: ()=>{calc.engine.putDigit(0)}},
        '1': {label: '1', onPress: ()=>{calc.engine.putDigit(1)}},
        '2': {label: '2', onPress: ()=>{calc.engine.putDigit(2)}},
        '3': {label: '3', onPress: ()=>{calc.engine.putDigit(3)}},
        '4': {label: '4', onPress: ()=>{calc.engine.putDigit(4)}},
        '5': {label: '5', onPress: ()=>{calc.engine.putDigit(5)}},
        '6': {label: '6', onPress: ()=>{calc.engine.putDigit(6)}},
        '7': {label: '7', onPress: ()=>{calc.engine.putDigit(7)}},
        '8': {label: '8', onPress: ()=>{calc.engine.putDigit(8)}},
        '9': {label: '9', onPress: ()=>{calc.engine.putDigit(9)}},
        'neg': {label: '±', onPress: ()=>{calc.engine.neg()}},
        'add': {cname: 'kopr', label: '+', onPress: ()=>{calc.engine.setOperation('add')}},
        'sub': {cname: 'kopr', label: '–', onPress: ()=>{calc.engine.setOperation('sub')}},
        'mul': {cname: 'kopr', label: '×', onPress: ()=>{calc.engine.setOperation('mul')}},
        'div': {cname: 'kopr', label: '÷', onPress: ()=>{calc.engine.setOperation('div')}},
        'pow': {cname: 'kopr', label: 'xⁿ', onPress: ()=>{calc.engine.setOperation('pow')}},
        'del': {cname: 'kfun', label: 'C', onPress: ()=>{calc.engine.del()}, onLongpress: ()=>{calc.engine.clear(true)}},
        'mem': {cname: 'kfun', label: 'M', onPress: ()=>{calc.engine.loadMemory()}, onLongpress: ()=>{calc.engine.setMemory()}, status: ()=>calc.engine.memory},
        'ret': {cname: 'kret', label: '=', onPress: ()=>{calc.engine.equals()}, doubleWidth: true},
    };
    
    let layout = [
        'del', 'mem', 'pow', 'mul',
         '7' ,  '8' ,  '9' , 'div',
         '4' ,  '5' ,  '6' , 'add',
         '1' ,  '2' ,  '3' , 'sub',
         '0' , 'neg',    'ret'
    ];
    
    calc.addIndicator(
        ()=>calc.engine.reduced ? 'RED' : 'NOR',
        ()=>{calc.engine.toggleFormat();}
    );
    
    calc.addIndicator(
        ()=>'MOD '+calc.engine.alu.mod,
        ()=>{calc.engine.setModulo()}
    );
    
    for(let k in keys){
        calc.registerButton({id: k, ...keys[k]});
    }
    
    calc.loadLayout(4, layout);
}

document.addEventListener('DOMContentLoaded', ()=>{
    let calc = new CalcEngine();
    let savestate = localStorage.getItem('calcdata');
    calc.loadState(savestate);
    
    document.addEventListener('visibilitychange', ()=>{
        if(document.hidden)
            localStorage.setItem('calcdata', calc.storeState());
    });
    
    let ui = new CalcUI(calc);
    loadDefaultCalc(ui);
    document.body.appendChild(ui.container);
    
    // for debug
    window.ui = ui;
    window.calc = calc;
});
