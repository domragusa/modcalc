let cur_version = 'cache_v190515.1';
 
let url_to_cache = [
    '/modcalc/',
    '/modcalc/index.html',
    '/modcalc/manifest.json',
    '/modcalc/style.css',
    '/modcalc/js/main.js',
    '/modcalc/js/engine.js',
    '/modcalc/js/ui.js',
    '/modcalc/images/icon-512.png',
    '/modcalc/images/icon-192.png',
    '/modcalc/images/icon.svg'
];


self.addEventListener('install', (e)=>{
    e.waitUntil(
        caches.open(cur_version).then((cache)=>cache.addAll(url_to_cache))
    );
});

self.addEventListener('fetch', (e)=>{
    e.respondWith(
        caches.match(e.request).then(
            (r)=> r || fetch(e.request)
        )
    );
});

self.addEventListener('activate', (e)=>{
    e.waitUntil(caches.keys().then(
        (names)=>Promise.all(
            names.map((cur)=>{
                if(cur != cur_version)
                    return caches.delete(cur);
            })
        )
    ));
});
