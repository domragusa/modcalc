ModCalc is a calculator implementing modular arithmetic; it supports addition, subtraction, multiplication, division and exponentiation.

https://drtiny.gitlab.io/modcalc/

# UI

## Display

The display is composed of three parts:

1. header, used to tell the current status (error, waiting for input, or result);
2. main value, for the input value or the result;
3. status, containing indicators for current modulo and remainder format.

Note: status indicators can be interacted with to set or toggle the relative value.

## Keypad

The keypad has:

1. digits and ± to change the current value;
2. operations (+, –, ×, ÷, xⁿ) to load the current value and set the operation;
3. equal key (=) to execute the calculation or load the value if none is set;
4. delete key (C) to remove a digit or clear on *longpress* or after and error;
5. memory key (M) to load the memory as the current value or set the memory on *longpress*.

Note: the memory key has a small led-like indicator that lights up when there's a non-zero value in memory.

## Errors

* NOINV, the value is not invertible mod m (note: the inverse is needed for division or exponentiation by a negative number).
* UNDEF, the calculation produced an undefined value (ex. 0^0).
* BADMOD, the value is not a valid modulo to use (it's less than 2).

